# ---
require esipressuregauge

epicsEnvSet(IOCNAME, "e03-vincitransducer-001")
epicsEnvSet(IPADDR, "172.30.32.57")
epicsEnvSet(IPPORT, "4001")
epicsEnvSet(PORTNAME, "PortA")
epicsEnvSet(PREFIX, "SE-SEE:SE-vincitransducer-001:")
epicsEnvSet(STREAM_PROTOCOL_PATH, "$(esipressuregauge_DIR)db")
epicsEnvSet(PM8006_ADDR, "F7")

iocshLoad("$(esipressuregauge_DIR)/esiPressureGauge.iocsh")
# ...
