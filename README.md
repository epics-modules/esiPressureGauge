
esiPressureGauge
======
European Spallation Source ERIC Site-specific EPICS module : esiPressureGauge

Additonal information:

* [Documentation](https://confluence.esss.lu.se/display/IS/Vinci+HP+syringe+pump)
* [Release notes](RELEASE.md)
* [Requirements](https://gitlab.esss.lu.se/e3-recipes/esiPressureGauge-recipe/-/blob/master/recipe/meta.yaml#L18)
* [Building and Testing](https://confluence.esss.lu.se/display/IS/1.+Development+locally+with+e3+and+Conda#id-1.Developmentlocallywithe3andConda-BuildanEPICSmoduleandtestitlocally)